#! /bin/bash

BTDEV=/dev/rfcomm0;

# kill any exsisting jack ghosts
#killall -9 jackd qjackctl

# turn on bluetooth device
blueman-manager &
sleep 1;
killall blueman-manager;

## bind bluetooth to /dev/rfcomm0
## requires password / sudo

if  test -e $BTDEV ; then
	echo "BLUETOOTH ALREADY BOUND!"
else gnome-terminal --title=BLUDETOOTHBIND -- rfcomm bind 0 10:97:BD:36:D6:4E;

fi;

# start jackd
if  [ $(pidof jackd) ] ; then
        echo "jackd ALREADY RUNNING"
else echo "starting jackd";  gnome-terminal --title=JACKD SERVER -- su -c ./start_jackd.sh rob;

fi;

sleep 1;

# start node server
echo "starting wavey wind" ;
gnome-terminal --title=NODE SERVER -- node wavey-wind/server.js /dev/rfcomm0 &&

echo "starting supercollider" ;
sh -c 'su -c 'scide gtr.scd'  rob &&' ;

# start guitarix
if  [ $(pidof guitarix) ] ; then
        echo "GUITARIX ALREADY RUNNING"
else echo "starting guitarix";  sh -c 'su -c "guitarix" rob' ;
fi;

echo "making jack connections" ;
su -c jack_connect system:capture_1 gx_head_amp:in_0 rob &&
su -c jack_connect gx_head_fx:out_0 SuperCollider:in_1 rob &&
