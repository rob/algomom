
import oscP5.*;
import netP5.*;
import http.requests.*;


OscP5 oscP5;

float x; // global variable
float y;
float z;

float ry;
int[] r = {3,5,8,13,21};

PShape chickenfoot;
//String urosC02;

import http.requests.*;
JSONObject json; // this is the full json object including the headers
JSONArray totalData; // this is the array in the feeds part
JSONObject record; // this is each one of the small json objects in the array
int id; // this is just a counter provided by Thingspeak that increments with each new posted data point. It is an int
String timeStamp; // this is the full time stamp provided by Thingspeak. It could be parsed to extract specific information for visualization
int temp; // this is the data point that I uploaded from my arduino 
// (a TMP36 analog temperature sensor --> Arduino Uno --> Ethernet Shield --> Thingspeak) using a standard arduino upload program
int co2; 
float humidity;

PFont liberation;
     
        
        
// https://api.thingspeak.com/channels/1724587/fields/3.json?results=1
void setup() {
  
  // UROS DATA FROM THINGSPEAK
  json = loadJSONObject("https://api.thingspeak.com/channels/1724587/feeds.json");
  totalData = json.getJSONArray("feeds");        
  for (int i = 0; i < totalData.size(); i++) { // step through the array, one record at a time
     record = totalData.getJSONObject(i); // remember the items in the array are also JSON objects
     id = record.getInt("entry_id"); // the key "entry_id" is given by Thingspeak. This is essentially a counter
     timeStamp = record.getString("created_at"); // the key "created_at" is also given by Thingspeak. I will write a parser to unpack it into a Date class soon.
     temp = int(record.getString("field1")); // Thingspeak stored this as a string so I have to convert it to an int for visualisation
     co2 = int(record.getString("field3")); // Thingspeak stored this as a string so I have to convert it to an int for visualisation
     humidity = int(record.getString("field4"));
     println(id + ", " + timeStamp + ", " + temp +", co2 level ppm: " + co2); // Just checking that it is all there.
          }
  
  liberation = createFont("LiberationMono-Bold.ttf", 45);

  chickenfoot = loadShape("chicken.obj");
  fullScreen(P3D);
 
  //size(400, 300);
  frameRate(24);
  //background(0);
  smooth();

  OscProperties properties = new OscProperties();
  properties.setListeningPort(47120); // osc receive port (from sc)
  oscP5 = new OscP5(this, properties);
}

void oscEvent(OscMessage msg) {
  if (msg.checkAddrPattern("/sc3p5")) {
    x = msg.get(0).floatValue(); // receive floats from sc
  }
  if (msg.checkAddrPattern("/mother2")) {
    y = msg.get(0).floatValue(); // receive floats from sc
  }
    if (msg.checkAddrPattern("/rotation")) {
    z = msg.get(0).floatValue(); // receive floats from sc
  }
}

void drawTarget(float xloc, float yloc, float size, float num) {
  float grayvalues = 255/num;
  float steps = size/num;
  ellipseMode(CENTER);
  for (int i = 0; i < num; i++) {
    fill(i*grayvalues);
    ellipse(xloc, yloc, size - i*steps, size - i*steps);
    }
  }
//https://forum.processing.org/two/discussion/4448/downloading-data-from-thingspeak.html

void draw() {
  
  if (y == 3){
      background(0); 
    } else { background(255); }
  
  // DISPLAY UROS DATA /////////////////
  
  textSize(22);
  fill(0, 111);
  textFont(liberation);
  
  text("Time: " + timeStamp, width*0.5, height/4);
  text("Temperature (Celsius): " + temp, width*0.5, height/5);
  text("CO2 PPM: " + co2, width*0.5, height/6);
  text("Humidity  (%rel): " + humidity, width*0.5, height/7);

  
  /////////////////
  
  lights();
  
  println("rotation: ", z);
  //rectMode(CORNERS);
  drawTarget(width/6 , (height/8) *-y + random(0,10) +height*0.7 , random(60, 1200), (y%8)+2);
  //scale(7.5);
   //rotateX(PI*z/6);  

  //drawTarget(width/12 * x, height/2,  222, x);
  //drawTarget(width*0.75, y, 120, 6);
  //background(x, x, x);
  println("POST: ", x);
  // draw rect
  //stroke(256-x/2, 256-x*abs(sin(x)), 256-x/4);
  //sdtkeWeight(random(12));
  fill(256-x/2, 256-x, 256-x*abs(sin(x)));
  //translate(width/2, height/2);
  //rotate(-x); //make recieve osc
  //rect(x%64, x%64, x*abs(sin(x))%128, x*abs(sin(x))%128, 6);
  //fill(random(255));
  rect(width/12 * x, 0, r[int(random(2))], height, 6);
  //translate(width/2, 0);
    if (y == 4){
  rotateX(PI*z/2);
    } else { 
        rotateX(0);
  }
    
  //rotateX(0.5);
  fill(255);
  rect(0, (height/8) *-y  +height*0.7 , width, random(0, 22), 6);
  translate(width/2, height/2 + 100, -200);
  rotateZ(PI);
  rotateY(ry);
  scale(8);
  fill(255, 255,0);
  shape(chickenfoot);
  
  ry += 0.03;
}
