import oscP5.*;
import netP5.*;
import http.requests.*;
import peasy.*;

PeasyCam cam;
OscP5 oscP5;

float x; // global variable
float y;
float z;

int col;
float rot;

float rx;
float ry;
float rz;

int[] r = {3,5,8,13,21,34,55,89};

PShape chickenfoot;
//String urosC02;

JSONObject json; 
JSONObject jsonchannel; 

JSONArray totalData; 
JSONArray nameData; 
JSONArray chnameData; 
JSONObject record;

int id; 
String name; 

String timeStamp; 
int temp; 
int co2; 
float humidity;
int s; // seconds counter

PFont liberation;
PGraphics urosBuffer;
boolean urosDisplayed;    
       
void setup() {
  fullScreen(P3D);
  colorMode(HSB, 255);
  liberation = createFont("LiberationMono-Bold.ttf", 30);
  frameRate(24);
  smooth();
  
  // SET OSC LISTEN FROM SUPERCOLLIDER
  OscProperties properties = new OscProperties();
  properties.setListeningPort(47120); // osc receive port (from sc)
  oscP5 = new OscP5(this, properties);
}

// SET OSC RECEIVE HANDLES (FROM SUPERCOLLIDER PATTERNS)
void oscEvent(OscMessage msg) {
      println("message: " + msg);
  if (msg.checkAddrPattern("/mother1")) {
    x = msg.get(0).floatValue(); // receive floats from sc
    println("xxxx: " + x);
  }
  if (msg.checkAddrPattern("/mother2")) {
    y = msg.get(0).floatValue(); // receive floats from sc
  }
    if (msg.checkAddrPattern("/rotation1")) {
    z = msg.get(0).floatValue(); // receive floats from sc
  }
}

////////////////////////////////////////////////////////

void draw() {

  if (y < 5 ){
      background(255, 0, 0);
      stroke(0);
      //background(123,44,234); 
      //background(0); 
    } else { 
      background(0);
      stroke(255);  }
  
  // UROS 
  int s = second(); 
  s = s - 60; // countdown timer
  if (s==-1) { // every minute
    thread("getUROS"); // threaded pull of UROS data from thingspeak
  }
  
 // scale(3);
  //urosDrawData(); // draw the data into a imagebuffer
   //if(urosDisplayed) { // use boolean to draw or not
  //image(urosBuffer,0,0); // display the imageBuffer
  //    }
  
  //////////////////////////////////////
  
  //lights();

  strokeWeight(2);
  fill(255);
  pushMatrix();
  //translate(width/2, 0);
  //rotateX(radians(22));
  rect(width/12 * x, 0, r[int(random(8))], height, 6); // MOTHER 1 VERTICAL
  popMatrix();
  
  //  if (y == 4){
  //rotateX(PI*z/2);
  //  } else { 
  //      rotateX(0);
  //}
    
  // VERTICAL SCALE DEGREE TRACKER
  
  fill(255);
  pushMatrix();
  //rotateY(radians(45));
  strokeWeight(2);
  rect(0, (height/8) *-y  +height*0.7 , width, r[int(random(8))], 6);
  popMatrix();

  // SPHERE //////////////////////////
  
  pushMatrix();
  translate(width *0.5, height * 0.5, 0);
  rotateX(radians( ry ));
  rotateY(radians( ry ));
  rotateZ(radians( ry ));
  strokeWeight(col % 2000 / 10);
  noFill();
  stroke(col%255, 100, 200);
  sphereDetail(5);
  sphere((col%1000 /10) * 150);
  popMatrix();

  // COUNTERS
  
  ry += 0.06;
  rx += 0.025;
  rz += 0.5;
  col += 4;
  rot += 0.5;

}
// END //////////////////////////
////////////////////////////////////////////////////
